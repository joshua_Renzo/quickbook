<?php
require_once (__DIR__) . '/config.php';
?>
<title>Connect to Quick Books Online</title>
<div>
    <?php if (!$quickbooks_is_connected): ?>
        <ipp:connectToIntuit></ipp:connectToIntuit>
    <?php endif; ?>
    <?php if ($quickbooks_is_connected): ?>
        <div><a href="http://crm.devtac.test/custom/QuickBookEntryPoint/disconnect.php">Disconnect from QuickBooks</a>
        </div>
        <br>
        <div>
            <button id="syncCustomers">Sync Customers</button>
        </div>
        <br>
        <div>
            <button id="syncInvoices">Sync Invoices</button>
        </div>
        <br>
        <div>
            <button id="syncPayments">Sync Payments</button>
        </div>
        <ipp:blueDot></ipp:blueDot>
        <div id="asas"></div>
    <?php endif; ?>
</div>
<script type="text/javascript" src="https://app center.intuit.com/Content/IA/intuit.ipp.anywhere.js"></script>
<script type="text/javascript">
    intuit.ipp.anywhere.setup({
        menuProxy: '<?php echo $quickbooks_menu_url; ?>',
        grantUrl: '<?php echo $quickbooks_oauth_url; ?>'
    });
    $document.getElementById("syncCustomers").onclick(function{
        var customerData = [
            <?php
            $CustomerService = new QuickBooks_IPP_Service_Customer();
            $customers = $CustomerService->query($Context, $realm, "SELECT * FROM Customer");
            foreach ($customers as $customer) {

                $refObj = new ReflectionObject($customer);
                $refProp = $refObj->getProperty('_data');
                $refProp->setAccessible(TRUE);
                $current = $refProp->getValue($customer);
                $id = $current['Id'];


                $tel = $current['PrimaryPhone'][0];
                $reftel = new ReflectionObject($tel);
                $telProp = $reftel->getProperty('_data');
                $telProp->setAccessible(TRUE);
                $primaryTel = $telProp->getValue($tel);
                $primaryTel = $primaryTel['FreeFormNumber'][0];



                $GLOBALS['log']->fatal(print_r($customer,true));
                //check if record exists
                //if yes then update
                //add if not
            }
            ?>
        ];
    });
    $document.getElementById("syncInvoices").onclick(function{

    });
    $document.getElementById("syncPayments").onclick(function{

    });
</script>